PROJECT_NAME := "go-sudoku-solver"
PKG := "gitlab.com/gautierenaud/$(PROJECT_NAME)"
EXEC := "cmd/go-sudoku-solver.go"
PKG_LIST := $(shell go list $(PKG)/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: all dep build clean test coverage coverhtml lint

all: build

lint: ## Lint the files
	@golint -set_exit_status ${PKG_LIST}

test: ## Run unittests
	@go test -short ${PKG_LIST}

race: dep ## Run data race detector
	@go test -race -short ${PKG_LIST}

msan: dep ## Run memory sanitizer
	@go test -msan -short ${PKG_LIST}

coverage: ## Generate global code coverage report
	@rm -f cover/*
	./scripts/coverage.sh;

coverhtml: coverage ## Generate global code coverage report in HTML
	go tool cover -html=cover/coverage.cov -o cover/coverage.html

dep: ## Get the dependencies
	@go get -v -d ./...
	@go get -u golang.org/x/lint/golint

build: dep ## Build the binary file
	@go build -i -v ${EXEC}

clean: ## Remove previous build
	@rm -f $(PROJECT_NAME)
	@rm -f cover/*

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

