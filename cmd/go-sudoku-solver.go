package main

import (
	"gitlab.com/gautierenaud/go-sudoku-solver/internal/sudoku"
)

// Main method of the project
func main() {
	sudokuGrid := sudoku.CreateEmptyGrid()
	sudoku.PrintGrid(sudokuGrid)
}
