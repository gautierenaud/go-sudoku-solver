package sudoku

import (
	"fmt"
	"testing"
)

type testSet struct {
	candidates         []bool
	expectedCandidates []int
}

func TestGetValidCandidates(t *testing.T) {
	testSets := []testSet{
		{[]bool{true, true, true, true}, []int{1, 2, 3}},
		{[]bool{false, true, true, true}, []int{1, 2, 3}},
		{[]bool{false, true, false, true}, []int{1, 3}},
		{[]bool{false, false, false, false}, []int{}},
	}

	for i := range testSets {
		testSet := testSets[i]
		validCandidates := getValidCandidates(testSet.candidates)
		expectedCandidates := testSet.expectedCandidates
		if len(validCandidates) != len(expectedCandidates) {
			t.Errorf("Incorrect number of candidates : %d", len(validCandidates))
		}
		for i := range validCandidates {
			if validCandidates[i] != expectedCandidates[i] {
				t.Errorf("Wrong candidate: %d instead of %d", validCandidates[i], expectedCandidates[i])
			}
		}
	}
}

func getCleanCandidates() []bool {
	candidates := make([]bool, 10)
	for i := range candidates[1:] {
		candidates[i+1] = true
	}

	return candidates
}

func TestSelectCandidate(t *testing.T) {
	for i := 1; i < 10; i++ {
		candidates := getCleanCandidates()
		selectCandidate(candidates, i)

		fmt.Println(candidates)

		if !candidates[0] {
			t.Errorf("The first boolean should be true")
		}

		for j := 1; j < 10; j++ {
			if j == i && !candidates[j] {
				t.Errorf("The choosen one should be true (%d, %d)", i, j)
			} else if j != i && candidates[j] {
				t.Errorf("The other should be false (%d, %d)", i, j)
			}
		}
	}
}
