package sudoku

func getValidCandidates(candidates []bool) []int {
	var validCandidates []int
	for i := range candidates[1:] {
		if candidates[i+1] {
			validCandidates = append(validCandidates, i+1)
		}
	}
	return validCandidates
}

func selectCandidate(candidates []bool, selectedCandidate int) {
	candidates[0] = true
	for i := range candidates[1:] {
		candidates[i+1] = false
	}
	candidates[selectedCandidate] = true
}