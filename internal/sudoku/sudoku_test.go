package sudoku

import "testing"

func TestCreateEmptyGrid(t *testing.T) {
	sudoku := CreateEmptyGrid()
	for i := range sudoku.grid {
		for j := range sudoku.grid[i] {
			if sudoku.grid[i][j][0] {
				t.Errorf("There should be no candidate yet")
			}

			for k := range sudoku.grid[i][j][1:] {
				if !sudoku.grid[i][j][k+1] {
					t.Errorf("%d should be a suitable candidate", k+1)
				}
			}
		}
	}
}
