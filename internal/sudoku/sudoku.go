package sudoku

import "fmt"

// Sudoku represents a matrix of the possible candidates
type Sudoku struct {
	grid [][][]bool
}

// CreateEmptyGrid create and returns a grid where everything is possible
func CreateEmptyGrid() Sudoku {
	grid := make([][][]bool, 9)
	for i := range grid {
		grid[i] = make([][]bool, 9)
		for j := range grid[i] {
			grid[i][j] = make([]bool, 10)
			for k := range grid[i][j] {
				grid[i][j][k] = true
			}
			grid[i][j][0] = false
		}
	}
	return Sudoku{grid}
}

// PrintGrid will print the grid
func PrintGrid(sudoku Sudoku) {
	for i := range sudoku.grid {
		for j := range sudoku.grid[i] {
			if sudoku.grid[i][j][0] {
				fmt.Print(getValidCandidates(sudoku.grid[i][j])[0])
			} else {
				fmt.Print("?")
			}
		}
		fmt.Println()
	}
}
